// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

import { connectDatabase } from '../database';
import { people } from '../StaticData/peopleData'

const seed = async () => {
  try {
    console.log('[seed]: database seed process in progress...');

    const db = await connectDatabase();

    for (const person of people) {
      await db.people.insertOne(person);
    }

    console.log('[seed]: database seeding was successful');
  } catch {
    throw new Error('There was a problem with database seeding.');
  }
}

seed();
