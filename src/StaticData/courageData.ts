import {
  FACEBOOK, TWITTER, SLACK, WEBSITE, BRITISH_COLUMBIA, ALBERTA,
  SASKATCHEWAN, MANITOBA, ONTARIO, QUEBEC, NOVA_SCOTIA,
  VICTORIA, VANCOUVER, CALGARY, SASKATOON, REGINA, 
  WINNEPEG, TORONTO, OTTAWA, MONTREAL, HALIFAX,
  DOCUMENT_FILE, SLIDES_FILE, SPREADSHEET_FILE, DIRECTORY, EMAIL,
  COMMS_CHANNEL, COURAGE, COURAGE_VICTORIA, COURAGE_VANCOUVER,
  COURAGE_CALGARY, COURAGE_SASKATOON, COURAGE_REGINA,
  COURAGE_WINNEPEG, COURAGE_TORONTO, COURAGE_OTTAWA,
  COURAGE_MONTREAL, COURAGE_HALIFAX, IMAGE_FILE, GOOGLE_DRIVE, PDF_FILE, PNG_FILE,
} from '../constants';

import { PoliticalOrganization } from '../lib/Types';

export const courageData: any = { // - TODO: -> Turn this back to PoliticalOrganization once these UUIDs are refactored to MongoDB's ObjectIds
  id: 'e2f399e0-51f4-488d-9392-289ec5628b88',
  name: COURAGE,
  description: "A pan-Canadian, membership-based organization that attempts to bridge the divide between movement and electoral politics. A space for learning, teaching, and collaboration; for planning and engaging in collective political action.",
  avatarURL: 'https://pbs.twimg.com/profile_images/846060121121832963/ypt0EFSR_400x400.jpg',
  socials: {
    id: 'af974c6e-efe7-4dad-aa58-3f3a76d038df',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: '381524c9-6ae0-4fb7-a448-5b965360108f',
        name: FACEBOOK,
        link: 'https://www.facebook.com/couragecoalition/',
      },
      {
        id: '780af0c9-d9b7-4c02-8f69-e0e410480d92',
        name: TWITTER,
        link: 'https://twitter.com/Courage_CA',
      },
      {
        id: 'bc598dce-5441-4d3e-92c7-54ab7b425057',
        name: WEBSITE,
        link: 'http://www.couragecoalition.ca',
      }
    ],
  },
  communications: {
    id: '2c39143c-b96f-42bc-a981-4de28ddcc9e0',
    name: 'Communications',
    types: [DIRECTORY],
    children: [
      {
        id: '6927e541-a491-4382-bed8-f6740d305331',
        name: SLACK,
        link: 'https://www.independentleft.slack.com',
        types: [DIRECTORY],
        children: [
          {
            id: '7eff70b4-c0e2-4044-9cd7-9edff8bb31c4',
            name: 'Slack Channels',
            types: [DIRECTORY],
            children: [
              {
                id: '7349846d-ccb4-46f1-928e-8919a3a48144',
                name : '#admin',
                types: [COMMS_CHANNEL]
              },
              {
                id: '85287fef-34e5-4938-81b8-ae1ba0ab2de9',
                name : '#alberta',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'f8a3165b-cb90-49bf-94ed-a2d19072cdb1',
                name : '#anti-imperialism',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'f9817e6f-7e92-43b2-98f6-9b1a21586ecc',
                name : '#antio-mandate',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'f9817e6f-7e92-43b2-98f6-9b1a21586ecc',
                name : '#basisofunity',
                types: [COMMS_CHANNEL]
              },
              {
                id: '30f17a97-22f5-4f81-9d62-de5c654cb857',
                name : '#branches',
                types: [COMMS_CHANNEL]
              },
              {
                id: '319ba672-5fd7-4bed-b2d9-c11f228b243b',
                name : '#campus',
                types: [COMMS_CHANNEL]
              },
              {
                id: '2d4e0dae-6aac-4827-81bf-1bfda218201c',
                name : '#citizens-assemblies',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'cf3e93ec-08b1-46e9-9298-710f2693e7c7',
                name : '#climate-energy',
                types: [COMMS_CHANNEL]
              },
              {
                id: '79573263-fd16-499a-be5c-ecb08f9fe91b',
                name : '#content',
                types: [COMMS_CHANNEL]
              },
              {
                id: '73ee9681-cd34-47fd-b0c7-4a7eedf708a5',
                name : '#couragepodcastmtl',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'e94ff664-c0b4-435e-beba-7978b1a54ef2',
                name : '#covid-19',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'a4ba2a97-9aa3-4847-a448-7bb8944bffb9',
                name : '#data-viz',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'b735e414-3643-4e25-b980-ef5ba7706891',
                name : '#dominoes',
                types: [COMMS_CHANNEL]
              },
              {
                id: '2c98fa8e-2922-4850-9a33-09038d669d12',
                name : '#electoralcommittee',
                types: [COMMS_CHANNEL]
              },
              {
                id: '04c68956-8c7b-44df-bf62-1ebb0069335e',
                name : 'enfrancaissvp',
                types: [COMMS_CHANNEL]
              },
              {
                id: '11b714a8-f74d-4e2f-8e1d-055bd6daf138',
                name : 'general',
                types: [COMMS_CHANNEL]
              },
              {
                id: '5f3395a4-2096-4d3a-aa83-69002c9d6d79',
                name : 'gm-oshawa',
                types: [COMMS_CHANNEL]
              },
              {
                id: '0de3716d-afb3-4233-92a5-d7ea42017c3d',
                name : '#governance',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'd566381c-ecfe-4703-b197-182995874408',
                name : '#greennewdeal',
                types: [COMMS_CHANNEL]
              },
              {
                id: '258e437d-6528-40ff-8655-0e07a049fd24',
                name : '#halifax',
                types: [COMMS_CHANNEL]
              },
              {
                id: '1b38fefd-300b-4617-8d1b-24ebe810728b',
                name : '#indigenous-issues',
                types: [COMMS_CHANNEL]
              },
              {
                id: '65b928da-d4bb-4699-9f48-8bb07763bec2',
                name : '#lgbtq2siaa',
                types: [COMMS_CHANNEL]
              },
              {
                id: '832748eb-9a4b-4182-b58e-59a5bcaa579c',
                name : '#manitoba',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'd6488a9b-e4ee-4090-a758-f671bcc5979d',
                name : '#memes-shitposting',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'abfd62b1-ac7b-454a-baae-cc52736d4f88',
                name : '#montreal',
                types: [COMMS_CHANNEL]
              },
              {
                id: '1ad4d788-5ae1-4892-8504-0c957ca6018a',
                name : '#mtlelectioncommittee',
                types: [COMMS_CHANNEL]
              },
              {
                id: '15648ede-e88c-4abf-81b9-29b015f9792f',
                name : '#mtllongterm',
                types: [COMMS_CHANNEL]
              },
              {
                id: '3edc3fdc-d8e1-44b8-9fc6-39698e0d6194',
                name : '#music',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'e6c6d4ce-a0c1-49d6-87f7-baa3e496fe5d',
                name : '#ndp',
                types: [COMMS_CHANNEL]
              },
              {
                id: '4845c931-a479-4a20-a9f2-43629667799f',
                name : '#ndp-ridings',
                types: [COMMS_CHANNEL]
              },
              {
                id: '25befb83-6952-4836-913f-3d579682f63b',
                name : '#newmembers',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'a135f8df-37eb-4db4-b37a-91121bdc48b2',
                name : '#newsandinfo',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'ea2bf2e2-bc42-474a-b78f-473be654084b',
                name : '#nondudes',
                types: [COMMS_CHANNEL]
              },
              {
                id: '398cf6f6-4006-44f7-b646-1dd60bd23a13',
                name : '#ondp',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'd7068aeb-078e-47aa-84eb-a3b6a22fdc37',
                name : 'ott_crisis_response',
                types: [COMMS_CHANNEL]
              },
              {
                id: '21e6708a-c55c-470c-84b5-a7efdccb4ee1',
                name : '#ott_elect_socialists',
                types: [COMMS_CHANNEL]
              },
              {
                id: '1f1c8a07-b82b-4c26-9408-743456002942',
                name : '#ott_environment_organizing',
                types: [COMMS_CHANNEL]
              },
              {
                id: '69faaaf5-6142-4fde-8d4a-870fcb0034d7',
                name : '#ott_housing',
                types: [COMMS_CHANNEL]
              },
              {
                id: '7ac49cfd-39f4-4d40-9547-af75f6177930',
                name : '#ott_intsolidarity',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'b2d96442-2c52-42c4-a6fc-20f22b0fa4a7',
                name : '#ott_member_engagement',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'd81e8b02-1cab-48fd-9eb9-33a0e01d1486',
                name : '#ott_outreach',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'fdcb7867-3c7b-4695-ba18-187953bce0df',
                name : '#ott_reading_group',
                types: [COMMS_CHANNEL]
              },
              {
                id: '3f84e4ae-bc4a-4a55-8a96-69cd7b844283',
                name : '#ott_transit',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'ae0f2b32-adef-4420-ac79-84bc177b278e',
                name : '#ottawa',
                types: [COMMS_CHANNEL]
              },
              {
                id: '7222afa5-acf3-4048-8b85-ea5b6ef28feb',
                name : '#racesolidarity',
                types: [COMMS_CHANNEL]
              },
              {
                id: '73bfae3d-def3-4582-a31f-565dfaa7c4e4',
                name : '#reading',
                types: [COMMS_CHANNEL]
              },
              {
                id: '8a8e8dcd-5f48-47e3-b96c-9d4c59f34623',
                name : '#recruitmentmtl',
                types: [COMMS_CHANNEL]
              },
              {
                id: '8c6ae232-2334-490e-acb8-f906dbd14932',
                name : '#reports',
                types: [COMMS_CHANNEL]
              },
              {
                id: '073bd681-c204-4913-848d-572d6e4ba681',
                name : '#saskabush',
                types: [COMMS_CHANNEL]
              },
              {
                id: '25ab8e25-909a-4cb1-9b33-455d1d340a72',
                name : '#socialmedia',
                types: [COMMS_CHANNEL]
              },
              {
                id: '3dceb2d7-af2b-449a-a19b-649cdb008142',
                name : '#structureinternaldemocracy',
                types: [COMMS_CHANNEL]
              },
              {
                id: '7559cb11-ea37-42d3-bbd8-6680b1a239a6',
                name : '#survivorsupport',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'c2cd3c55-6011-4aca-91e2-ecab5045ee57',
                name : '#training',
                types: [COMMS_CHANNEL]
              },
              {
                id: '70cedee1-8c6e-4639-80d8-a0f3c7be206b',
                name : '#unite-the-left',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'd449a998-daf7-4f8e-b796-623b3c5a5c38',
                name : '#web',
                types: [COMMS_CHANNEL]
              },
              {
                id: '1fb62bd8-85e0-405e-9820-45209e916a80',
                name : '#westcoast',
                types: [COMMS_CHANNEL]
              },
              {
                id: '6bd910f6-c716-4a2d-aa6c-662cd6935bbf',
                name : '#wethenorth',
                types: [COMMS_CHANNEL]
              },
              {
                id: '19ad19ab-e65a-4487-a477-ee6e6e090c87',
                name : '#yyz',
                types: [COMMS_CHANNEL]
              },
              {
                id: '47627a14-5107-4f8b-b522-044ab4c5a424',
                name : '#yyz-mutual-aid',
                types: [COMMS_CHANNEL]
              },
            ]
          }
        ]
      },
      {
        id: '091d403b-5680-4208-b014-9814f22803d3',
        name: EMAIL,
        link: 'mailto:ottawa@couragecoalition.com',
        value: 'ottawa@couragecoalition.ca',
      },    
    ],
  },
  resources: {
    id: 'bde260a2-d5a7-43d3-94dd-ebb29bd6d5a1',
    name: 'Resources',
    types: [DIRECTORY],
    children: [
      {
        id: 'b491571a-249d-40b5-ad63-ecda21092dc5',
        name: 'Basis of Unity',
        types: [DOCUMENT_FILE],
        description: "Presents the organization's core values, which are agreed upon, upheld, and supported.",
        link: 'http://www.couragecoalition.ca/en-unity/',
      },
      {
        id: '522c277f-8279-44ac-b634-6b1ffd85ba3a',
        name: 'Frequently Asked Questions',
        shorthand: 'FAQ',
        types: [DOCUMENT_FILE],
        link: 'http://www.couragecoalition.ca/en-faq/',
      },
      {
        id: '4f1c4eb5-2fa5-4c2b-bccc-cbf2628fac4e',
        name: 'About',
        types: [DOCUMENT_FILE],
        link: 'http://www.couragecoalition.ca/about/',
      },
      {
        id: 'e6f89e1f-6949-405a-acd1-1275b92ce9c4',
        name: 'Design Resources',
        types: [DIRECTORY],
        link: 'https://drive.google.com/drive/folders/1X4EXArTAJuM67mcdGbNKegT7Gi8eode4?usp=sharing',
        children: [
          {
            id: '3451f1ce-756d-4b51-b541-81ee0972ca90',
            name: 'Design Resources Folder',
            types: [GOOGLE_DRIVE],
            link: 'https://drive.google.com/drive/folders/1X4EXArTAJuM67mcdGbNKegT7Gi8eode4?usp=sharing'
          },
          {
            id: '076792bf-57e6-4ffd-8506-b899ee55438b',
            name: 'Courage Color Palette',
            types: [IMAGE_FILE],
            link: 'https://drive.google.com/file/d/1qHrhxVfHlFssHFbu0uuX08gT4YeaefNt/view?usp=sharing'
          },
        ]
      },
    ],
  },
  workingGroups: [
    {
      id: '722d29c0-95c0-49c2-b129-4fb450735fa6',
      name: 'Member Engagement',
      coChairs: [

      ],
      members: [

      ],
      socials: {
        id: 'e3469f80-b0f0-4a1b-a823-4c817fd48df7',
        name: 'Socials',
        types: [DIRECTORY],
        children: [

        ]
      },
      communications: {
        id: '4b2f23e6-f19f-4bf7-ba8b-656b9b19a04a',
        name: 'Communications',
        children: [
          {
            id: '1c0e8960-7f84-4b5d-ab96-422e3d5c3c2e',
            name: 'Slack',
            link: 'https://www.independentleft.slack.com',
            children: [
              {
                id: '7d2ddcd2-0dfe-4eaa-b722-16f94474489a',
                name: 'Slack Channels',
                children: [
                  {
                    id: '9becdb0e-1b6c-406a-ba92-cb67db0a0112',
                    name: '#project_labor_organizing',
                  }
                ]
              }
            ],
          }
        ],
      },
      resources: {
        id: '99f176d9-8f01-4044-8374-2c53aded7a97',
        name: 'Resources',
        types: [DIRECTORY],
        children: [
          {
            id: 'c83cd4cd-d15a-4862-a258-4aad0f3d014c',
            name: 'Labour Organizing Project - Shared Drive',
            types: [GOOGLE_DRIVE],
            link: 'https://drive.google.com/drive/folders/1SwiSvIJrMyxJkdv9x7nwaMm5R9GRun8F'
          },
          {
            id: 'd62f7341-05f8-452b-a074-2f566f387d92',
            name: 'Labor Organizing Projects - Resources',
            types: [SPREADSHEET_FILE],
            link: 'https://docs.google.com/spreadsheets/d/1v_nDX7sD1VCfZNJutq_EB9TWXoEkfsZelQyrp_RZet0/edit?usp=sharing',
          }
        ]
      }
    }
  ],  
  chapters: [
    // --------------------- //
    // -> COURAGE VICTORIA - //
    // --------------------- //
    {
      id: 'c9d7ba7a-81c5-4bdf-be31-c1cd39e07e20',
      name: COURAGE_VICTORIA,
      location: VICTORIA,
      province: BRITISH_COLUMBIA,
      avatarURL: 'https://pbs.twimg.com/profile_images/861661665829339136/vcDldF65_400x400.jpg',
      socials: {
        id: '0081ffb2-085a-43d6-b51f-ec0983fb1a4b',
        name: 'Socials',
        types: [DIRECTORY],
        children: [
          {
            id: '61d5a05b-f155-4328-a6c1-9d6f36d608ce',
            name: TWITTER,
            link: 'https://twitter.com/Courage_VIC',
          }
        ]
      },
      communications: {
        id: '116e7bf6-adff-416a-8bf5-665e654e2539',
        name: 'Communications',
        types: [DIRECTORY],
        children: [

        ]
      },
      resources: {
        id: '15b61413-6cf0-4e4f-8b67-100c49faa497',
        name: 'Resources',
        types: [DIRECTORY],
        children: [

        ]
      },
      workingGroups: [
        // {
        //   id: '',
        //   name: '',
        //   coChairs: [
        //     { id: '', name: '' }
        //   ],
        //   members: [
        //     { id: '', name: '' },
        //   ],
        //   resources: [
        //     {
        //       id: '',
        //       name: '',
        //       types: [],
        //       link: ''
        //     }
        //   ],
        // }
      ],
    },
    // ---------------------- //
    // -> COURAGE VANCOUVER - //
    // ---------------------- //
    {
      id: '4b8c85db-3baa-447e-b60e-ee480310626f',
      name: COURAGE_VANCOUVER,
      location: VANCOUVER,
      province: BRITISH_COLUMBIA,
      avatarURL: 'https://pbs.twimg.com/profile_images/861657807333937152/Ui5advoo_400x400.jpg',
      socials: {
        id: 'c87f2c86-c6d1-4760-8678-7fb143117605',
        name: 'Socials',
        types: [DIRECTORY],
        children: [
          {
            id: 'a7b4acc3-f214-47b1-95f4-5eb239e08244',
            name: TWITTER,
            link: 'https://twitter.com/Courage_VAN',
          }
        ]
      },
      communications: {
        id: '1871665a-d9a8-4571-bd1e-a037326210a3',
        name: 'Communications',
        types: [DIRECTORY],
        children: [

        ]
      },
      resources: {
        id: '9af93593-253e-4317-b7fc-67f9ca5d2090',
        name: 'Resources',
        types: [DIRECTORY],
        children: [

        ]
      },
      workingGroups: [
        // {
        //   id: '',
        //   name: '',
        //   coChairs: [
        //     { id: '', name: '' }
        //   ],
        //   members: [
        //     { id: '', name: '' },
        //   ],
        //   resources: [
        //     {
        //       id: '',
        //       name: '',
        //       types: [],
        //       link: '',
        //     }
        //   ],
        // }
      ],      
    },
    // -------------------- //
    // -> COURAGE CALGARY - //
    // -------------------- //
    {
      id: '0c04dbe2-1e89-444f-8cc0-ffde534d6116',
      name: COURAGE_CALGARY,
      location: CALGARY,
      province: ALBERTA,
      avatarURL: 'https://pbs.twimg.com/profile_images/862002736325316608/Xq1jTN6D_400x400.jpg',
      socials: {
        id: 'bb8181e0-d4f1-48b3-8d6d-f308159ba97b',
        name: 'Socials',
        types: [DIRECTORY],
        children: [
          {
            id: '3e7cc784-be09-47b8-be7c-c2007892e35f',
            name: TWITTER,
            link: 'https://twitter.com/Courage_CGY',
          }
        ],
      },
      communications: {
        id: '6974b758-1d20-41e2-bab8-21965447812b',
        name: 'Communications',
        types: [DIRECTORY],
        children: [

        ]
      },
      resources: {
        id: 'c46f9f2a-9ee7-4ae9-b122-48d25bc0a9fe',
        name: 'Resources',
        types: [DIRECTORY],
        children: [

        ]
      },
      workingGroups: [
        // {
        //   id: '',
        //   name: '',
        //   coChairs: [
        //     { id: '', name: '' }
        //   ],
        //   members: [
        //     { id: '', name: '' },
        //   ],
        //   resources: [
        //     {
        //       id: '',
        //       name: '',
        //       types: [],
        //       link: '',
        //     }
        //   ],
        // }
      ],      
    },
    // ---------------------- //
    // -> COURAGE SASKATOON - //
    // ---------------------- //
    {
      id: '7d95ae38-eb97-40d7-bb5a-c2a9e3348fb9',
      name: COURAGE_SASKATOON,
      location: SASKATOON,
      province: SASKATCHEWAN,
      avatarURL: 'https://pbs.twimg.com/profile_images/861666943404724224/wHtPHmRK_400x400.jpg',
      socials: {
        id: '79585e0b-cbe8-4f90-a0f6-9f3add74bd10',
        name: 'Socials',
        types: [DIRECTORY],
        children: [
          {
            id: '7bf9796b-e260-4788-87d3-aece4e5921d4',
            name: TWITTER,
            link: 'https://twitter.com/Courage_SSK',
          }
        ]
      },
      communications: {
        id: 'ec2a16aa-bebd-47d1-9009-2e5712ff2848',
        name: 'Communications',
        types: [DIRECTORY],
        children: [

        ]
      },
      resources: {
        id: 'cc1e1c49-0dfd-45e0-833b-0bbe5bebd326',
        name: 'Resources',
        types: [DIRECTORY],
        children: [

        ]
      },
      workingGroups: [
        // {
        //   id: '',
        //   name: '',
        //   coChairs: [
        //     { id: '', name: '' }
        //   ],
        //   members: [
        //     { id: '', name: '' },
        //   ],
        //   resources: [
        //     {
        //       id: '',
        //       name: '',
        //       types: [],
        //       link: '',
        //     }
        //   ],
        // }
      ],      
    },
    // ------------------- //
    // -> COURAGE REGINA - //
    // ------------------- //
    {
      id: '981d5f06-39ba-4aa6-805a-434a24ee380c',
      name: COURAGE_REGINA,
      location: REGINA,
      province: SASKATCHEWAN,
      avatarURL: 'https://pbs.twimg.com/profile_images/861664087389818882/uwHLqC54_400x400.jpg',
      socials: {
        id: 'cb616b08-91b5-4cbf-895f-a6816bf79bda',
        name: 'Socials',
        types: [DIRECTORY],
        children: [
          {
            id: 'da59618f-1e36-43b4-84e2-cb848d902774',
            name: TWITTER,
            link: 'https://twitter.com/Courage_REG',
          }
        ]
      },
      communications: {
        id: 'd064811f-dead-4881-b3ee-aa38e601b499',
        name: 'Communications',
        types: [DIRECTORY],
        children: [

        ]
      },
      resources: {
        id: 'd685228a-91c9-40de-9251-59cf36781b2b',
        name: 'Resources',
        types: [DIRECTORY],
        children: [

        ]
      },
      workingGroups: [
        // {
        //   id: '',
        //   name: '',
        //   coChairs: [
        //     { id: '', name: '' }
        //   ],
        //   members: [
        //     { id: '', name: '' },
        //   ],
        //   resources: [
        //     {
        //       id: '',
        //       name: '',
        //       types: [],
        //       link: '',
        //     }
        //   ],
        // }
      ],      
    },
    // --------------------- //
    // -> COURAGE WINNEPEG - //
    // --------------------- //
    {
      id: 'ea65277e-154d-45d9-9ec6-d3c04f274be2',
      name: COURAGE_WINNEPEG,
      location: WINNEPEG,
      province: MANITOBA,
      avatarURL: 'https://pbs.twimg.com/profile_images/860589695931097088/MA7ZhXzn_400x400.jpg',
      socials: {
        id: '8a59ec4c-f266-4afe-8d4a-fc31c39407a3',
        name: 'Socials',
        types: [DIRECTORY],
        children: [
          {
            id: 'b111e290-1445-4df2-9401-44b67a9e7575',
            name: TWITTER,
            link: 'https://twitter.com/Courage_WPG',
          }
        ]
      },
      communications: {
        id: '5e0a73be-eb94-4868-b15c-3afbd33d9f89',
        name: 'Communications',
        types: [DIRECTORY],
        children: [

        ]
      },
      resources: {
        id: '471cf03e-ceb6-48f0-9051-22011c379bf5',
        name: 'Resources',
        types: [DIRECTORY],
        children: [

        ]
      },
      workingGroups: [
        // {
        //   id: '',
        //   name: '',
        //   coChairs: [
        //     { id: '', name: '' }
        //   ],
        //   members: [
        //     { id: '', name: '' },
        //   ],
        //   resources: [
        //     {
        //       id: '',
        //       name: '',
        //       types: [],
        //       link: '',
        //     }
        //   ],
        // }
      ],      
    },
    // -------------------- //
    // -> COURAGE TORONTO - //
    // -------------------- //
    {
      id: '60a86a4f-392b-4efe-b48d-98584e193847',
      name: COURAGE_TORONTO,
      location: TORONTO,
      province: ONTARIO,
      avatarURL: 'https://pbs.twimg.com/profile_images/860580763456028672/wFhYDaqD_400x400.jpg',
      socials: {
        id: '66551760-9cc2-478b-b87d-4d74b446d185',
        name: 'Socials',
        types: [DIRECTORY],
        children: [
          {
            id: '3c0f9991-5d68-4821-a505-fe90cff80fbb',
            name: TWITTER,
            link: 'https://twitter.com/Courage_TOR',
          }
        ]
      },
      communications: {
        id: 'c6c17ea1-0935-4ad3-b2ab-c3c018ff6906',
        name: 'Communications',
        types: [DIRECTORY],
        children: [

        ]
      },
      resources: {
        id: '3623135f-9972-4e19-a76a-933ac0527bf8',
        name: 'Resources',
        types: [DIRECTORY],
        children: [

        ]
      },
      workingGroups: [
        // {
        //   id: '',
        //   name: '',
        //   coChairs: [
        //     { id: '', name: '' }
        //   ],
        //   members: [
        //     { id: '', name: '' },
        //   ],
        //   resources: [
        //     {
        //       id: '',
        //       name: '',
        //       types: [],
        //       link: '',
        //     }
        //   ],
        // }
      ],      
    },
    // ------------------- //
    // -> COURAGE OTTAWA - //
    // ------------------- //
    {
      id: '39f49ec3-0bc7-499d-841f-905975813568',
      name: COURAGE_OTTAWA,
      location: OTTAWA,
      province: ONTARIO,
      // - TODO: -> Dynamically generate this value from the member list once it's filled in.
      memberCount: 30,
      members: [
        // { id: '', name: '' }
      ],
      avatarURL: 'https://pbs.twimg.com/profile_images/862002736325316608/Xq1jTN6D_400x400.jpg',
      socials: {
        id: '3e202336-ab2f-42ac-ad87-28da1b93ac2b',
        name: 'Socials',
        types: [DIRECTORY],
        children: [
          {
            id: '8fa51a7e-20f7-4ab1-90ec-41486040e70b',
            name: TWITTER,
            link: 'https://twitter.com/CourageOttawa',
          }
        ]
      },
      communications: {
        id: '010711cf-c6c9-4a2c-a7a4-21be312b19fe',
        name: 'Communications',
        types: [DIRECTORY],
        children: [
          {
            id: 'da30f52d-b28b-4190-81d3-4d480f8283a3',
            name: SLACK,
            link: 'https://www.independentleft.slack.com',
            children: [
              {
                id: 'b6210ccb-5793-4dbf-818e-7a7913c40c53',
                name: 'Slack Channels',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'd7068aeb-078e-47aa-84eb-a3b6a22fdc37',
                    name : '#ott_crisis_response',
                    types: [COMMS_CHANNEL]
                  },
                  {
                    id: '21e6708a-c55c-470c-84b5-a7efdccb4ee1',
                    name : '#ott_elect_socialists',
                    types: [COMMS_CHANNEL]
                  },
                  {
                    id: '1f1c8a07-b82b-4c26-9408-743456002942',
                    name : '#ott_environment_organizing',
                    types: [COMMS_CHANNEL]
                  },
                  {
                    id: '69faaaf5-6142-4fde-8d4a-870fcb0034d7',
                    name : '#ott_housing',
                    types: [COMMS_CHANNEL]
                  },
                  {
                    id: '7ac49cfd-39f4-4d40-9547-af75f6177930',
                    name : '#ott_intsolidarity',
                    types: [COMMS_CHANNEL]
                  },
                  {
                    id: 'b2d96442-2c52-42c4-a6fc-20f22b0fa4a7',
                    name : '#ott_member_engagement',
                    types: [COMMS_CHANNEL]
                  },
                  {
                    id: 'd81e8b02-1cab-48fd-9eb9-33a0e01d1486',
                    name : '#ott_outreach',
                    types: [COMMS_CHANNEL]
                  },
                  {
                    id: 'fdcb7867-3c7b-4695-ba18-187953bce0df',
                    name : '#ott_reading_group',
                    types: [COMMS_CHANNEL]
                  },
                  {
                    id: '3f84e4ae-bc4a-4a55-8a96-69cd7b844283',
                    name : '#ott_transit',
                    types: [COMMS_CHANNEL]
                  },
                  {
                    id: 'ae0f2b32-adef-4420-ac79-84bc177b278e',
                    name : '#ottawa',
                    types: [COMMS_CHANNEL]
                  },
                ]
              }
            ]
          },
          {
            id: 'dfce6b40-e041-45d8-a842-84f7387f5b9f',
            name: EMAIL,
            value: 'ottawa@couragecoalition.ca',
            link: 'mailto:ottawa@couragecoalition.ca',
          },        
        ],
      },
      resources: {
        id: '6ddaec4d-cdaf-48ae-90a5-a9ee0b8146aa',
        name: 'Resources',
        types: [DIRECTORY],
        children: [
          {
            id: 'a2103f63-2086-4903-bf3c-8ca7e73a543c',
            name: 'Working Groups Overview',
            types: [DOCUMENT_FILE],
            link: 'https://docs.google.com/document/d/1NX_fZu2SFy1gQF5eFMFHgebB4wthRNB6tA1vWTPKnU8/edit',
          },
          {
            id: '20deb67e-0ac6-40a1-9e5b-37b3dd38e022',
            name: 'Meeting Minutes',
            types: [DIRECTORY],
            children: [
              {
                id: '6d17dcb4-bf05-4c64-891d-d371373047d8',
                name: 'Meeting Minutes Folder',
                types: [GOOGLE_DRIVE],
                link: 'https://drive.google.com/drive/folders/12BQPPnwCIyPto7BaB1FGfI7uwAcI_ivA?usp=sharing',
              },
              {
                id: 'd0426ffd-795e-495d-8396-e25e471753e2',
                name: 'Courage Ottawa Chapter Meeting - April 15, 2020 - Minutes',
                types: [DOCUMENT_FILE],
                link: 'https://drive.google.com/open?id=1tDR3LGwNHdIbFNl2mKVpR6KnRBoIp_bcfPnoFdMDo10'
              },
              {
                id: 'e32f26cd-65ed-40a3-9f72-2e8f2d9f53be ',
                name: 'Courage Ottawa Chapter Meeting - May 13, 2020 - Minutes',
                types: [DOCUMENT_FILE],
                link: 'https://drive.google.com/open?id=1AkaryeSumY5NS5-_KUHcS_skSmSCgVQkvckuYHHtIx0'
              },
            ]
          },
        ],
      },   
      workingGroups: [
        {
          id: 'e027dce0-722a-46f2-8516-2b3a6e5e177c',
          name: 'Climate Justice / GND',
          description: '',
          coChairs: [
            { id: '5e88faf4-263d-4d3f-b98c-0534c3b1c317', name: 'Silas Xuereb' }
          ],
          members: [
            { id: '5e88faf4-263d-4d3f-b98c-0534c3b1c317', name: 'Silas Xuereb' },
            { id: '74df8f54-9892-492a-b4c5-7bce88605e45', name: 'Fred Langlois' },
            { id: 'b1df823f-6da4-4660-9c30-a8fc7a34adfb', name: 'Gabriel Bourget' },
          ],
          socials: {
            id: '',
            name: '',
          },
          communications: {
            id: '08ebbce3-3b20-42bd-86e9-ae988f698bdb',
            name: 'Communications',
            types: [DIRECTORY],
            children: [
              {
                id: '76a20383-e9ea-46c5-a08f-f8a62465ba9a',
                name: 'Slack',
                link: 'https://www.independentleft.slack.com',
                children: [
                  {
                    id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
                    name: 'Slack Channels',
                    children: [
                      {
                        id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                        name: '#ott_environment_organizing'
                      }
                    ]
                  }
                ],
              }
            ],
          },
          resources: {
            id: '4a4e8841-a544-4cc4-8d46-190249f90d01',
            name: 'Resources',
            types: [DIRECTORY],
            children: [
              {
                id: '2981f548-2e9e-4ed8-b094-7b673c690000',
                name: 'Organization Tracking - Climate Justice / GND Working Group',
                types: [SPREADSHEET_FILE],
                description: 'Spreadsheet containing structured information on groups of interest involved with climate justice, and related topics.',
                link: 'https://docs.google.com/spreadsheets/d/1FLBkzyzczzgJt2RNfKeDGGJCePNTNoentF9J7sb3I8k/edit#gid=0',
              },
              {
                id: '',
                name: '',
                description: '',
                link: '',
              },
            ],
          },
        },
        {
          id: 'bbd8140c-a65d-4ddb-9200-bb06b99eefb3',
          name: 'Electing Socialists',
          description: '',
          coChairs: [
            { id: '31df33c0-c62c-4813-a1c2-e2993aae6536', name: 'James Hutt' },
            { id: 'b20de0d8-12f7-4b0b-b05e-0b3e1f625bac', name: 'Julia Szwarc' },
          ],
          members: [
            { id: '31df33c0-c62c-4813-a1c2-e2993aae6536', name: 'James Hutt' },
            { id: 'b20de0d8-12f7-4b0b-b05e-0b3e1f625bac', name: 'Julia Szwarc' },
            { id: '0e8c4dd5-a36f-4ea8-92d6-31fadaf314de', name: 'Matt Lipton' },
            { id: '154f29aa-1870-4119-a018-0de3edf2b87b', name: 'Clem' },
            { id: '26d1406a-e157-4bc1-a011-f27c4c2a2dc3', name: 'Meagan Wiper' },
            { id: '94c0fab7-be85-40ca-9391-a0afe5cd0865', name: 'Ethan Mitchell' },
          ],
          socials: {
            id: '9c333d00-7937-4fca-a2d8-69d6e54feb48',
            name: 'Socials',
          },
          communications: {
            id: '08ebbce3-3b20-42bd-86e9-ae988f698bdb',
            name: 'Communications',
            children: [
              {
                id: '76a20383-e9ea-46c5-a08f-f8a62465ba9a',
                name: 'Slack',
                link: 'https://www.independentleft.slack.com',
                children: [
                  {
                    id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
                    name: 'Slack Channels',
                    children: [
                      {
                        id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                        name: '#ott_elect_socialists'
                      }
                    ]
                  }
                ],
              }
            ],
          },
          resources: {
            id: '6d92aef4-148d-4a3e-a1d4-8aeb5cc77f17',
            name: 'Resources',
            types: [DIRECTORY],
            children: [
              {
                id: '43f4b427-a19e-40d6-ae47-9cb8727462dc',
                name: 'The NDP: A Marxist Analysis (1997)',
                types: [DOCUMENT_FILE, PDF_FILE],
                link: 'https://drive.google.com/file/d/1iCeMCgpl0kNHjKf7NZwgkLPckEwqu2Zl/view',
              }
            ]
          },
        },
        {
          id: '49da41fb-dbfd-4fe0-9d45-7fa08d678815',
          name: 'Housing',
          coChairs: [
            { id: 'f674ec89-d44c-4432-833e-0d58e41997ab', name: 'Sam Hersh' },
          ],
          members: [
            { id: 'f674ec89-d44c-4432-833e-0d58e41997ab', name: 'Sam Hersh' },
            { id: '6d1406a-e157-4bc1-a011-f27c4c2a2dc3', name: 'Meagan Wiper' },
          ],
          communications: {
            id: 'cca30a9d-9fd3-457a-9cdf-ac9493a4cf45',
            name: 'Communications',
            children: [
              {
                id: '76a20383-e9ea-46c5-a08f-f8a62465ba9a',
                name: 'Slack',
                link: 'https://www.independentleft.slack.com',
                children: [
                  {
                    id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
                    name: 'Slack Channels',
                    children: [
                      {
                        id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                        name: '#ott_housing'
                      }
                    ]
                  }
                ],
              }
            ],
          },
          socials: {
            id: '6e5ca161-4096-47ca-98bb-588d492fe223',
            name: 'Socials',
          },
          resources: {
            id: '451bfb1d-fdbb-463f-8119-05dc927d85c8',
            name: 'Resources',
            types: [DIRECTORY],
            children: [
              {
                id: '',
                name: '',
                description: '',
                link: '',
              }
            ]
          },
        },
        {
          id: 'f3971cc5-f0e2-4b43-b80d-d08893e1dea6',
          name: 'International Solidarity',
          coChairs: [
            { id: '44ffdba4-9885-4c71-8078-174797126512', name: 'Shivangi Misra' },
          ],
          members: [
            { id: '44ffdba4-9885-4c71-8078-174797126512', name: 'Shivangi Misra' },
            { id: 'c6a1f621-882e-4304-8bb7-273b986184b9', name: 'David K' },
            { id: 'ec2cf87e-0a03-4efd-b921-9161cde9fe67', name: 'Tyler Paziuk' },
            { id: 'acb74f0e-cf61-40d9-9d7f-4ae59b5f9917', name: 'Debbie Owusu-Akyeeah' },
            { id: 'a9d4b6ef-6a7d-4b17-aa87-d4de5d8379d1', name: 'Adrian Murray' },
            { id: '26d1406a-e157-4bc1-a011-f27c4c2a2dc3', name: 'Meagan Wiper' },
          ],
          communications: {
            id: 'cc878bfd-25ea-42ad-917d-4f5aeffa12ce',
            name: 'Communications',
            children: [
              {
                id: '76a20383-e9ea-46c5-a08f-f8a62465ba9a',
                name: 'Slack',
                link: 'https://www.independentleft.slack.com',
                children: [
                  {
                    id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
                    name: 'Slack Channels',
                    children: [
                      {
                        id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                        name: '#ott_intsolidarity'
                      }
                    ]
                  }
                ],
              }
            ],
          },
          socials: {
            id: 'ecb258e5-36d8-42c9-8dda-2ac12536388f',
            name: 'Socials',
          },
          resources: {
            id: 'e4aa4514-6a2f-4a2c-8e34-8dad83e4f261',
            name: 'Resources',
            types: [DIRECTORY],
            children: [
              {
                id: 'c51a515d-e8fc-4da5-a625-b88ad3f3e1af',
                name: 'International Solidarity Contacts',
                types: [SPREADSHEET_FILE],
                description: 'List containing individuals and organizations to stay in contact with in the context of international solidarity.',
                link: 'https://docs.google.com/spreadsheets/d/11N9sFQOqHuiSNieTlOmPDT9Nnd-9vjx-SX_qfcF5gOk/',
              }
            ]
          },          
        },
        {
          id: 'd1e67b4a-9e0d-40dc-bf3f-1c4ddff3c19c',
          name: 'Mobility Justice',
          coChairs: [
            { id: 'ec2cf87e-0a03-4efd-b921-9161cde9fe67', name: 'Tyler Paziuk' },
            { id: 'af43048e-05a6-45c8-b729-13943bf00411', name: 'Cameron Roberts' },
          ],
          members: [
            { id: 'ec2cf87e-0a03-4efd-b921-9161cde9fe67', name: 'Tyler Paziuk' },
            { id: 'af43048e-05a6-45c8-b729-13943bf00411', name: 'Cameron Roberts' },
            { id: 'b20de0d8-12f7-4b0b-b05e-0b3e1f625bac', name: 'Julia Szwarc' },
            { id: '6c711d48-219a-4ed6-a78b-cad5e2ba54c3', name: 'Laura Shantz' },
            { id: 'c40ee152-a057-49e9-a506-faabadf21798', name: 'Sam B' },
            { id: 'ee9492c9-1dcc-4f59-bb0a-d41099e42705', name: 'Karim A' },
            { id: '2dbbc389-d5cc-4ac1-b261-e3d0748fa7fa', name: 'Melanie Mathias' },
          ],
          socials: {
            id: 'b0730a5a-e5a1-4190-b8b4-3c63880a0cfa',
            name: 'Socials',
          },
          communications: {
            id: 'b9076346-cb53-48cd-b5ae-f7865aabe41a',
            name: 'Communications',
            children: [
              {
                id: '76a20383-e9ea-46c5-a08f-f8a62465ba9a',
                name: 'Slack',
                link: 'https://www.independentleft.slack.com',
                children: [
                  {
                    id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
                    name: 'Slack Channels',
                    children: [
                      {
                        id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                        name: '#ott_transit',
                      }
                    ]
                  }
                ],
              }
            ],
          },
          resources: {
            id: '4fdf080c-7012-47f7-898d-43d6e12401ab',
            name: 'Resources',
            types: [DIRECTORY],
            children: [
              {
                id: '1c1e3620-585d-4f4d-9855-8d6c7055df35',
                name: 'Mobility Justice Working Group Google Drive',
                types: [GOOGLE_DRIVE],
                link: 'https://drive.google.com/drive/folders/1DhjSQ5PbFubeaR1ah2yX_oU4p0qAuxUf',
                description: '',
              },
              {
                id: '44de24e5-9fc3-4668-b57f-d49ef254d18d',
                name: 'ParaParity Campaign',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'edeab501-0394-40a5-a6ec-4e096a144ebf',
                    name: 'ParaParity Research',
                    types: [DOCUMENT_FILE],
                    link: 'https://docs.google.com/document/d/1JUlrM7j5X3tOb0_PpSHfBw1m_JR1GisZnjOQai8BjOU/edit?usp=sharing',
                  }
                ]
              },
              {
                id: '0bb22513-6d3f-41d7-b5bd-2f9caff44e72',
                name: 'James Wilt Webinar Event',
                types: [DIRECTORY],
                children: [
                  {
                    id: '51fbb3f2-cf79-4c87-97e9-b23ad112d8e4',
                    name: 'Welcome Slide',
                    types: [SLIDES_FILE],
                    link: 'https://docs.google.com/presentation/d/1cd2Z6rHh2kQ-lhlKrvv5FDd_l32vSx6JEjG-Drhusug/edit?usp=sharing'
                  },
                  {
                    id: 'b004232e-a063-4c1d-81f2-08df216c6819',
                    name: 'Welcome Slide v2',
                    types: [SLIDES_FILE],
                    link: 'https://docs.google.com/presentation/d/1-e54_j5d9VEWsu6qmEt_v367zjIJIxk3ntSGA-K3dtM/edit?usp=sharing',
                  },
                  {
                    id: '8427bb34-3bbe-41e1-a225-b1829f3860b7',
                    name: 'Opening Remarks',
                    types: [DOCUMENT_FILE],
                    link: 'https://docs.google.com/document/d/1ozMTbGppgHz20lEj87UGxuZi3ldQsyJ-sAkdT2JtRqg/edit?usp=sharing',
                  },
                  {
                    id: 'a0974e30-91dd-4e4d-82cb-9905a27623a8',
                    name: 'General Invitation',
                    types: [DOCUMENT_FILE],
                    link: 'https://docs.google.com/document/d/1HkhOLMVDCDiVdaB3H67fPpv_KeRmCWu5I08jzeabO2M/edit?usp=sharing',
                  },
                  {
                    id: '887aab0f-852e-4e3d-a1bd-326f1de7d13c',
                    name: 'Invitation for Elected Representatives',
                    types: [DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/124YPdXiUuADwg-g_5lSqRwIbjmPNeDkv/view?usp=sharing'
                  },
                  {
                    id: '2616ba80-20ef-4419-9dfc-eee7704c02aa',
                    name: 'Invitation for Elected Representatives v2',
                    types: [DOCUMENT_FILE],
                    link: 'https://docs.google.com/document/d/1PMIYpmoDBbIGyRTXg8dJZ2d4j3Mm3wRMGWyScZ7sXQ0/edit?usp=sharing',
                  },
                  {
                    id: 'f18cc82d-e681-422e-88b2-042b3e741987',
                    name: 'Twitter Promo 2160x1080',
                    types: [IMAGE_FILE, PNG_FILE],
                    link: 'https://drive.google.com/file/d/1wylt7uRWDAuWBfOgZ4egqiMUPCthD-XX/view?usp=sharing',
                  },
                  {
                    id: 'e68c3b80-2626-484d-b659-fd482aa0ba9e',
                    name: 'Instagram Promo Square',
                    types: [IMAGE_FILE, PNG_FILE],
                    link: 'https://drive.google.com/file/d/1mfPrvl_VynY_mLitanmZlPGAwx0CLXV0/view?usp=sharing',
                  },
                  {
                    id: '40b579ca-748d-48d7-acd7-4c1859291ff6',
                    name: 'Portrait Promo',
                    types: [IMAGE_FILE, PNG_FILE],
                    link: 'https://drive.google.com/file/d/1UaCt_EpjFYb_7ibRsChcRUawQ3UoOVXB/view?usp=sharing',
                  },
                ]
              },
              {
                id: '22d4ef9a-bd4d-4885-8a39-067d85ad8fe8',
                name: 'Social Media Shareables',
                types: [DIRECTORY],
                children: [
                  {
                    id: '40e8bc90-bc4c-4268-93fa-1e65a0a4fee5',
                    name: 'Beg Buttons',
                    types: [IMAGE_FILE, PNG_FILE],
                    link: 'https://drive.google.com/file/d/1lbjCfHUwbVa1IaTkEwHmZkwIF9dCPvbC/view?usp=sharing'
                  },
                  {
                    id: '5967d754-f5fd-4f09-a1c7-a0861748166d',
                    name: 'Maintain Bus Service',
                    types: [IMAGE_FILE, PNG_FILE],
                    link: 'https://drive.google.com/file/d/1fxIKk90hv_AWh8xi3RtcSNZeGaud6LXr/view?usp=sharing',
                  },
                  {
                    id: '941b4d70-20d8-4d6c-be23-28a0f4d312fd',
                    name: 'Free Parking and Safe Transit',
                    types: [IMAGE_FILE, PNG_FILE],
                    link: 'https://drive.google.com/file/d/10SECA93-kyk7Tl3w75lvcDK9pcsQQ-wu/view?usp=sharing',
                  },
                  {
                    id: '320e7409-d296-458c-8c41-ed3575846d7a',
                    name: 'More Space',
                    types: [IMAGE_FILE, PNG_FILE],
                    link: 'https://drive.google.com/file/d/1zJPJjs0O-rcBvmqlJYthacMkN-aRnuYO/view?usp=sharing',
                  }
                ]
              },
              {
                id: 'e1d61828-530a-4a3e-9cbf-ad32de361350',
                name: 'Courage Comms Committee Motion',
                types: [DOCUMENT_FILE],
                link: 'https://docs.google.com/document/d/1X6hBIaovQNh5GXPxLAQQx446I1oCIcGdOAgel21WpL4/edit?usp=sharing'
              },
              {
                id: '0d8bbfa7-cb8b-4217-98af-cc0e47bcd79e',
                name: 'Courage Mobility Justice Working Group - Our Vision for Ottawa',
                types: [DOCUMENT_FILE],
                link: 'https://docs.google.com/document/d/1TtMLT4ztMdrfPLHj6kDH8gr1QgdDHY7SXj9VfuRGoVE/edit?usp=sharing'
              },
              {
                id: 'fa057b59-77b5-4f24-8597-4498119e0a7e',
                name: 'Meetings',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'd4cbae29-dcc2-4e5a-b281-7326a9d47a21',
                    name: '2020',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: '3e830a98-dcea-4292-99a4-eae2e4e41016',
                        name: 'March',
                        types: [DIRECTORY],
                        children: [
                          {
                            id: '263dc2ab-1531-4e81-8268-f047fe42baee',
                            name: 'March 30',
                            types: [DIRECTORY],
                            children: [
                              {
                                id: '26dacfd5-15e6-4b1a-b35b-92089d6ff010',
                                name: 'COMJWG - March 30 - Meeting Agenda',
                                types: [DOCUMENT_FILE],
                                link: 'https://docs.google.com/document/d/1vFaWgEDV3XmS958oYlWTCeySM2Tsz3RnZFN_X8vnrww/edit?usp=sharing',
                              },
                              {
                                id: '160a2f9a-b747-4cee-b45c-8f0c4bd1479e',
                                name: 'COMJWG - March 30 - Meeting Minutes',
                                types: [DOCUMENT_FILE],
                                link: 'https://docs.google.com/document/d/1B37OTqZ_DgaiYXesxJ5Q5BPC8SE_1zBS2HFu6q27940/edit?usp=sharing'
                              }
                            ]
                          }
                        ]
                      },
                      {
                        id: 'b5d7dbe5-0ce1-4861-b631-0803b6f4ac4e',
                        name: 'April',
                        types: [DIRECTORY],
                        children: [
                          {
                            id: 'ad645f46-a9c5-4502-bb4d-6bac3f92128f',
                            name: 'April 29',
                            types: [DIRECTORY],
                            children: [
                              {
                                id: 'b1069685-29f9-4945-834a-095c5fff3913',
                                name: 'COMJWG - April 29 - Meeting Agenda',
                                types: [DOCUMENT_FILE],
                                link: 'https://docs.google.com/document/d/1xARMeCYxcYNCcpkZghsFl1uC-ObcS7LlgAEj1cJTdfE/edit?usp=sharing'
                              }
                            ]
                          }
                        ]
                      },
                      {
                        id: '49bf38bc-16d6-4824-8696-ac8e622f90f3',
                        name: 'May',
                        types: [DIRECTORY],
                        children: [
                          {
                            id: 'bf116df0-558d-45eb-9c4e-6540da4d5346',
                            name: 'May 19',
                            types: [DIRECTORY],
                            children: [
                              {
                                id: 'cc32515a-fa38-4019-a366-6f02d207a324',
                                name: 'COMJWG - May 19 - Meeting Agenda',
                                types: [DOCUMENT_FILE],
                                link: 'https://docs.google.com/document/d/1GqS8Qisl86eHywA5oXy2RUk6Ow1FcD3QUTRoysgm7Cs/edit?usp=sharing'
                              }
                            ]
                          },
                        ]
                      },
                    ]
                  }
                ]
              },
              {
                id: '85a53fa7-cd80-4577-be92-ec7b1db16ece',
                name: 'Free Transit In Ottawa - Pamphlet (June 2018)',
                types: [DOCUMENT_FILE],
                link: 'https://drive.google.com/file/d/13Ixx_5PtQpEISs37ulz3vEtwzLYfuU2V/view?usp=sharing'
              },
              {
                id: '8fc05b7c-e1e4-4b32-b56e-2fd5e7e450b9',
                name: 'Letter to Jim Watson - May 15, 2020',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'cda931e5-d2fc-4154-a9ef-271988acbf2f',
                    name: 'Open Letter to Jim Watson - May 15, 2020',
                    types: [DOCUMENT_FILE],
                    link: 'https://docs.google.com/document/d/14jSdRgLfzmOWFL_9cMZw8MR5ei5gKetpt2AYl0fthSg/edit?usp=sharing'
                  },
                  {
                    id: 'fe23fa7a-322a-4051-8f3e-910886063b27',
                    name: 'Open Letter to Jim Watson - May 15, 2020',
                    types: [PDF_FILE, DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/1-1pPW6Oxv7FBTNedM9SLYFy7mloAKDep/view?usp=sharing',
                  }
                ]
              },
              {
                id: 'd612834e-eaa3-46db-be0d-243a041273cc',
                name: 'COMJWG - Demands of Possible Allies',
                types: [DOCUMENT_FILE],
                link: 'https://docs.google.com/document/d/1y4NbZQM_ciSeAGJ-s8bvMVWQh0oxKkEsDC45nUn68FA/edit?usp=sharing',
              },
              {
                id: '5f1f60cb-bb33-49e6-b0ff-b57a479cac90',
                name: 'Works In Progress (WIP)',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'cc344122-c61a-4204-91c4-2ef996b6b0fc',
                    name: 'Courage Federal Public Transit Working Paper',
                    types: [PDF_FILE, DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/1iAOFZfU4-ch_DRDXVrTO6K25q3WGtHHq/view?usp=sharing'
                  }
                ]
              }
            ]
          },
        },
        {
          id: 'a73c7fb5-f6d0-4cc1-a5e0-2d15501b423c',
          name: 'Reading Group',
          coChairs: [
            // { id: '', name: '' },
          ],
          members: [
            // { id: '', name: '' },
          ],
          socials: {
            id: 'ea28b4b7-dc8a-49da-af61-207679582109',
            name: 'Socials',
            children: [

            ]
          },
          communications: {
            id: 'b9076346-cb53-48cd-b5ae-f7865aabe41a',
            name: 'Communications',
            children: [
              {
                id: '2688ee0a-00e3-42e7-8c3b-e42c0f5d1ccf',
                name: 'Slack',
                link: 'https://www.independentleft.slack.com',
                children: [
                  {
                    id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
                    name: 'Slack Channels',
                    children: [
                      {
                        id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                        name: '#ott_reading_group',
                      }
                    ]
                  }
                ],
              }
            ],
          },
          resources: {
            id: 'a022913b-cd43-4cfa-9a13-8cf5ee070082',
            name: 'Resources',
            types: [DIRECTORY],
            children: [
              {
                id: '17659fc9-4432-4e65-8739-ceae685da4b5',
                name: 'Readings',
                types: [DIRECTORY],
                children: [
                  {
                    id: '4b213a7e-927c-45f4-a856-986779e8679d',
                    name: 'Books',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: '414856bc-7b33-4c61-8281-adf3e4dd1c0f',
                        name: 'Social Reproduction Theory: Remapping Class, Recentering Oppression - Tithi Bhattacharya',
                        types: [DIRECTORY],
                        children: [
                          {
                            id: '070e2496-a87e-4cdc-ba2e-a8b4d1fb45c5',
                            name: 'Social Reproduction Theory: Remapping Class, Recentering Oppression - Tithi Bhattacharya',
                            types: [PDF_FILE],
                            link: 'https://drive.google.com/file/d/1OnwRsoK8r8NQD2OT1D0q04TUL-JvYyIj/view',
                          },
                          {
                            id: 'a41a0dd2-3511-4ee2-8f4d-f36ef858eb25',
                            name: 'Chapter 2 - Notes - Gabriel Bourget',
                            types: [DOCUMENT_FILE],
                            link: 'https://drive.google.com/file/d/1SYviQmynKD7IyP_oNu_I0FNUD8p2vYeD/view?usp=sharing'
                          }
                        ],
                      },
                      {
                        id: '2874afc9-3cc5-495c-bb1b-d6bb9a3bf861',
                        name: 'The Wretched of The Earth - Frantz Fanon',
                        types: [DIRECTORY],
                        children: [
                          {
                            id: '304b0e76-416c-4cc4-a3fa-2be66b85f6bf',
                            name: 'The Wretched of The Earth - Frantz Fanon',
                            types: [PDF_FILE],
                            link: 'http://abahlali.org/wp-content/uploads/2011/04/Frantz-Fanon-The-Wretched-of-the-Earth-1965.pdf',
                          }
                        ]
                      }
                    ]
                  }
                ]
              },
              {
                id: 'e98d6332-b2cb-4de9-a3e1-bd9cbdd2d8ce',
                name: 'Black Socialists - Resource Guide',
                types: [WEBSITE],
                description: "Collection of readings and other resources in the valence of socialism, centered around a Black perspective.",
                link: 'https://blacksocialists.us/resource-guide',
              }
            ]
          },
        // },
        // {
        //   id: '',
        //   name: '',
        //   coChairs: [
        //     { id: '', name: '' },
        //   ],
        //   members: [
        //     { id: '', name: '' },
        //   ],
        //   resources: [
        //     {
        //       id: '',
        //       name: '',
        //       description: '',
        //       link: '',
        //     }
        //   ],
        },
      ],
    },
    // --------------------- //
    // -> COURAGE MONTREAL - //
    // --------------------- //
    {
      id: 'e7129e80-3e71-4a25-b900-5a2997acd1d5',
      name: COURAGE_MONTREAL,
      location: MONTREAL,
      province: QUEBEC,
      avatarURL: 'https://pbs.twimg.com/profile_images/862010072674598912/wMJnToWE_400x400.jpg',
      socials: {
        id: '3270e7e7-117b-4ca7-9b9b-4574236dc0a9',
        name: 'Socials',
        types: [DIRECTORY],
        children: [
          {
            id: '1c23c5aa-cdaf-4d16-8ee6-185dfbbb5f3b',
            name: TWITTER,
            link: 'https://twitter.com/Courage_MTL',
          }
        ],
      },
      communications: {
        id: 'ad3b0f80-3f3c-4116-a269-13cdd3418625',
        name: 'Communications',
        types: [DIRECTORY],
        children: [

        ]
      },
      resources: {
        id: '16dce0ea-8c31-4831-a77a-56e35c4fcd06',
        name: 'Resources',
        types: [DIRECTORY],
        children: [

        ]
      },
      workingGroups: [
        // {
        //   id: '',
        //   name: '',
        //   coChairs: [
        //     { id: '', name: '' }
        //   ],
        //   members: [
        //     { id: '', name: '' },
        //   ],
        //   resources: [
        //     {
        //       id: '',
        //       name: '',
        //       types: [],
        //       link: '',
        //     }
        //   ],
        // }
      ],      
    },
    // -------------------- //
    // -> COURAGE HALIFAX - //
    // -------------------- //
    {
      id: '5d6d6ff8-b8ea-4d2a-9bfe-a9e381b99035',
      name: COURAGE_HALIFAX,
      location: HALIFAX,
      province: NOVA_SCOTIA,
      avatarURL: 'https://pbs.twimg.com/profile_images/862015443908804608/6WdKo2of_400x400.jpg',
      socials: {
        id: '244de32c-3e47-4110-8619-09dce15f5d2d',
        name: 'Socials',
        types: [DIRECTORY],
        children: [
          {
            id: '40984bb2-1d3c-4166-b0d2-4f2738d623a0',
            name: TWITTER,
            link: 'https://twitter.com/Courage_MTL',
          }
        ]
      },
      communications: {
        id: '30de5f55-efb0-4349-ad7d-7d5ad8e7e739',
        name: 'Communications',
        types: [DIRECTORY],
        children: [

        ]
      },
      resources: {
        id: 'c7e9a383-6a43-4350-b5aa-77e00e837eaa',
        name: 'Resources',
        types: [DIRECTORY],
        children: [

        ]
      },
      workingGroups: [
        // {
        //   id: '',
        //   name: '',
        //   coChairs: [
        //     { id: '', name: '' }
        //   ],
        //   members: [
        //     { id: '', name: '' },
        //   ],
        //   resources: {
        //     name: 'Resources',
        //     types: [DIRECTORY],
        //     children: [
        //       {
        //         id: '',
        //         name: '',
        //         types: [],
        //         link: '',
        //       }
        //     ],
        //   }
        // }
      ],    
    },
  ], // - Chapters
};
