// -> Organization
export const COURAGE = 'Courage';
export const COURAGE_VICTORIA = 'Courage Victoria';
export const COURAGE_VANCOUVER = 'Courage Vancouver';
export const COURAGE_CALGARY = 'Courage Calgary';
export const COURAGE_SASKATOON = 'Courage Saskatoon';
export const COURAGE_REGINA = 'Courage Regina';
export const COURAGE_WINNEPEG = 'Courage Winnipeg';
export const COURAGE_TORONTO = 'Courage Toronto';
export const COURAGE_OTTAWA = 'Courage Ottawa';
export const COURAGE_MONTREAL = 'Courage Montreal';
export const COURAGE_HALIFAX = 'Courage Halifax';

// -> Links
export const COURAGE_WEBSITE_LINK = "http://www.couragecoalition.ca";
export const COURAGE_WEBSITE_ABOUT_LINK = "http://www.couragecoalition.ca/about/";
export const COURAGE_WEBSITE_BASIS_OF_UNITY_LINK = "http://www.couragecoalition.ca/en-unity/";
export const COURAGE_WEBSITE_FAQ_LINK = "http://www.couragecoalition.ca/en-faq/";
export const COURAGE_WEBSITE_MEMBERSHIP_LINK = "http://www.couragecoalition.ca/en-member-start/";
