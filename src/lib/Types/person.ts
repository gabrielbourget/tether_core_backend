import { ObjectId } from 'mongodb';
import { Resource } from './resource';

export type Person = {
  _id: ObjectId;
  firstName?: string;
  lastName?: string;
  name: string;
  chapter: {
    id: string;
    name: string;
  },
  avatarURL?: string;
  socials?: Resource;
  communications?: Resource;
}
