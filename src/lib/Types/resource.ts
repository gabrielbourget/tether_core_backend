import { ObjectId } from 'mongodb';

export type Resource = {
  _id: ObjectId;
  name: string;
  shorthand?: string;
  types?: string[];
  link?: string;
  value?: string;
  description?: string;
  children?: Resource[];
}
