import { Collection } from "mongodb";
import { Person } from './person'

export interface Database {
  people: Collection<Person>;
}
