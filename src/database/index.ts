import { MongoClient } from "mongodb";
import { Database, Person } from '../lib/Types';

const {
  DB_USER, DB_USER_PASSWORD,
  DB_CLUSTER, DB_NAME
} = process.env;

const url = `mongodb+srv://${DB_USER}:${DB_USER_PASSWORD}@${DB_CLUSTER}.mongodb.net/<dbname>?retryWrites=true&w=majority`;

export const connectDatabase = async (): Promise<Database> => {
  const client = await MongoClient.connect(
    url,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  );

  const db = client.db(DB_NAME);

  return {
    people: db.collection<Person>("test_people")
  }
};
