import merge from 'lodash.merge';
import { personResolvers } from './Person';

export const resolvers = merge(personResolvers);
