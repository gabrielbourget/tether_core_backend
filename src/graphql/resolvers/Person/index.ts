import { IResolvers } from 'apollo-server-express';
import { ObjectId } from 'mongodb';
import { Database, Person } from '../../../lib/Types';

export const personResolvers: IResolvers = {
  Query: {
    people: async (
      _root: undefined,
      _args: unknown,
      { db }: { db: Database }
    ): Promise<Person[]> => {
      return await db.people.find({}).toArray();
    }
  },
  Mutation: {
    deletePerson: async (
      _root: undefined,
      { id }: { id: string } ,
      { db }: { db: Database },
    ): Promise<Person> => {
        const deleteResponse = await db.people.findOneAndDelete({
          _id: new ObjectId(id),
        })

        if (!deleteResponse.value) {
          throw new Error(`There was a problem while trying to delete a person with id ${id}`)
        }
        return deleteResponse.value;
    }
  },
  Person: {
    id: (person: Person): string => person._id.toString()
  }
}
