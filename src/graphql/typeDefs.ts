import { gql } from 'apollo-server-express';

export const typeDefs = gql`
  type Resource {
    id: ID!
    name: String!
    shorthand: String
    types: [String]
    link: String
    value: String
    description: String
    children: [Resource]
  }

  type Person {
    id: ID!
    firstName: String
    lastName: String
    name: String!
    avatarURL: String
    socials: Resource
    communciations: Resource
  }

  type WorkingGroup {
    id: ID!
    name: String!
    description: String
    socials: Resource
    communications: Resource
    coChairs: [Person!]
    members: [Person!]
    resources: Resource
  }

  type CourageChapter {
    id: ID!
    name: String!
    location: String!
    province: String!
    avatarURL: String
    socials: Resource
    communications: Resource
    resources: Resource
    workingGroups: [WorkingGroup!]!
    members: [Person]
  }

  type PoliticalOrganization {
    id: ID!
    name: String!
    description: String
    avatarURL: String
    socials: Resource
    communications: Resource
    resources: Resource
    chapters: [CourageChapter!]!
  }

  type Query {
    # Resource, Person, WorkingGroup, CourageChapter
    people: [Person]
  }

  type Mutation {
  #   # create resource, update resource, delete resource
  #   # create person, update person, delete person
  #   # create working group, update working group, delete working group,
  #   # create chapter, update chapter, delete chapter
    deletePerson(id: ID!): Person!
  }
`;